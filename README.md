############################
# Les Samedi Programmation #
############################


Samedi 26/09/2020 : Scripting Space Engineers
=============================================


Ce code source est la base du live sur la gestion de la connectique véhicule / station pour vider le véhicule.


Il montre un exemple de gestion de pistons & connecteurs pour vider un véhicule, gérés par un script.

- Récupération d'objets dans le script
- Interactions
- Parcours de liste pour suivre l'état des objets (connecteur & piston)
- Détection de l'état pour avoir un comportement différent selon l'état en cours (non connecté, piston en cours de déploiement, connecté, etc)


Assistance au scripting :

https://github.com/malware-dev/MDK-SE/wiki/Quick-Introduction-to-Space-Engineers-Ingame-Scripts

https://github.com/malware-dev/MDK-SE/wiki/Api-Index


Si vous avez plus de questions, vous pouvez me contacter sur https://twitch.tv/adaralex

Bonne journée à vous,

Adaralex
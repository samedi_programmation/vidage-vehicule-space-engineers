public Program()
{
    Runtime.UpdateFrequency = UpdateFrequency.Once;
 //   Runtime.UpdateFrequency = UpdateFrequency.Update100;
}

public void Save()
{

}

public void ExtendPiston(IMyPistonBase piston)
{
       piston.Velocity = 0.3f;
       piston.Extend();  
}

public void RetractPiston(IMyPistonBase piston)
{
       piston.Velocity = 1.0f;
       piston.Retract();  
}

List<IMyPistonBase> GetConnectorPistons()
{
    List<IMyTerminalBlock> allBlocks = new List<IMyTerminalBlock>(); 
    GridTerminalSystem.GetBlocksOfType<IMyPistonBase>(allBlocks); // Where allBlocks is a list type.  

//    return allBlocks.ConvertAll(x => (IMyPistonBase)x);

    List<IMyPistonBase> result = new List<IMyPistonBase>();

    foreach(IMyTerminalBlock block in allBlocks) {
        if (block.CustomName == "Piston_Connecteur") {
            result.Add((IMyPistonBase) block);
        }

    }
    return result;
}

public void RetractAllPistons() {
    List<IMyPistonBase> allPistons = GetConnectorPistons();
    foreach(IMyPistonBase piston in allPistons) {
        RetractPiston(piston);
    }
}

public void ExtendAllPistons() {
    List<IMyPistonBase> allPistons = GetConnectorPistons();
    foreach(IMyPistonBase piston in allPistons) {
        ExtendPiston(piston);
    }
}


public void Main(string argument, UpdateType updateSource)
{
    List<IMyTerminalBlock> allBlocks = new List<IMyTerminalBlock>(); 
    GridTerminalSystem.GetBlocksOfType<IMyShipConnector>(allBlocks); // Where allBlocks is a list type.  

    IMyTextPanel EcranSamediProg = GridTerminalSystem.GetBlockWithName("Samedi_Prog_1") as IMyTextPanel;

    foreach(IMyTerminalBlock block in allBlocks) {

        IMyShipConnector connector = (IMyShipConnector) block;
        if (connector.CustomName != "Connecteur_Vehicule_Azo") {
            continue;
        }
           EcranSamediProg.WritePublicText("\n" + connector.Status, false);
        if (connector.Status.ToString() == "Connected") {
            // We're connected, we wanna leave
           EcranSamediProg.WritePublicText("\nConnected, let's leave", true);

            connector.ThrowOut = false;
            connector.Disconnect();
            RetractAllPistons();
            Runtime.UpdateFrequency = UpdateFrequency.None;
        } else if (connector.Status == MyShipConnectorStatus.Unconnected) {
           EcranSamediProg.WritePublicText("\nNot connected !", true);
            List<IMyPistonBase> pistons = GetConnectorPistons();
           EcranSamediProg.WritePublicText("\nTesting " + pistons.Count + " pistons...", true);
        
            foreach(IMyPistonBase piston in pistons) {
                EcranSamediProg.WritePublicText("\nPiston status: " + piston.Status, true);
                if (piston.Status.ToString() == "Extending") {
                    // Currently trying to connect, don't interfere
                    return ;
                } else if (piston.Status.ToString() == "Extended") {
                     RetractAllPistons();
                     Runtime.UpdateFrequency = UpdateFrequency.None;
                } else {
                    // Not trying to extend yet, let's extend !
                    ExtendAllPistons();
                    Runtime.UpdateFrequency = UpdateFrequency.Update100;
                }
            }
        } else {
            // Connectable status
           EcranSamediProg.WritePublicText("\nCan be connected, let's empty cargo", true);
            connector.Connect();
            connector.ThrowOut = true;
           Runtime.UpdateFrequency = UpdateFrequency.None;
        }
    }        
}
